---
author: Dimitri Robert
date: 18 avril 2020
title: Utiliser BigBlueButton
lang: fr-FR
toc: 1
toc-depth: 2
dpi: 300
papersize: a4
geometry:
- top=20mm
- bottom=20mm
- left=15mm
- right=15mm
links-as-notes: 1
colorlinks: 1
jquery: 1
javascript: ./actions.js

---

# Entrez dans le salon principal

Un salon est un espace virtuel BigBlueButton où vous pouvez vous connecter à plusieurs. Vous pourrez parler avec votre micro, partager votre webcam pour être vu par les autres, partager votre écran, discuter sur le tchat.

Vous aurez également la possibilité de joindre d'autres salons par plus petits groupes.

## Indiquez un nom

Vous avez reçu un lien d'accès à la plateforme BigBlueButton. Saisissez un nom dans le champ prévu à cet effet et cliquez sur Démarrer. Pas besoin de s'inscrire ni de s'authentifier.

![](./BBB-images/2020-04-03_17-14.png)

## Paramétrez le son

L'écran suivant vous demande si vous souhaitez « rejoindre l'audio ». Vous avez le choix entre :

* Microphone : activez votre micro pour pouvoir parler
* Écoute seule : votre micro sera muet et vous serez juste spectateur

Cet état peut être modifié une fois dans le salon.

![](./BBB-images/2020-04-03_17-19.png)

Si vous activez votre micro, BBB vous demande d'effectuer un test d'écho. Parlez et vérifiez que vous entendez votre voix par les haut-parleurs ou le casque. Vérifiez les niveaux de volume sonore (sortie et éventuellement entrée micro).

![](./BBB-images/2020-04-03_17-30.png)

C'est bon, vous êtes dans la place !

# Tour d'horizon du salon

## Qui est connecté ?

Le menu situé tout à gauche montre, entre-autres, la liste des utilisateurs. Il y a deux types de rôles :

* modérateur (icône carrée arrondie) : permet de tout gérer ;
* utilisateur normal (icône ronde)

![](./BBB-images/2020-04-03_18-43.png)

Notez également les petites icônes superposées :

* le tableau sur fond bleu en haut à gauche indique le présentateur ;
* le micro indique que l'utilisateur peut parler ;
* le casque montre un utilisateur spectateur.

## Discussion publique

Outre la voix il est possible de communiquer par écrit. La fenêtre « Discussion publique » est ouverte lors de votre connexion. Il est possible de la réduire et de l'afficher à nouveau via le menu en cliquant sur *Discussion publique*.

![](./BBB-images/2020-04-03_18-15.png)

Attention, la discussion n'est pas sauvegardée : elle est perdue lorsque le salon est fermé. Le menu de la discussion (via le bouton 3 points, dit *hamburger* en haut de la fenêtre) vous permet de :

* sauvegarder au format HTML
* copier dans le presse-papiers
* effacer

![](./BBB-images/2020-04-03_18-18.png)

## Notes partagées

Depuis le menu de gauche vous pouvez ouvrir la fenêtre des notes partagées. Similaire à un framapad simplifié, chaque participant peut rédiger en même temps.

![](./BBB-images/2020-04-03_20-16.png)

**Attention** ces notes seront perdues à la fermeture du salon. Si vous souhaitez les conserver il faut les exporter via le bouton *Importer/exporter*.

![](./BBB-images/2020-04-03_20-20.png)

## Présentations

La zone principale à droite est l'espace dédié à l'affichage des présentations.
Une seule personne à la fois peut animer la présentation, elle doit avoir le statut de « présentateur ». Un modérateur peut devenir présentateur de lui-même (le précédent perd alors automatiquement la main) et peut donner la main à un simple utilisateur.

Vous pouvez afficher une présentation (format PDF ou issue d'une suite bureautique, ODF et Microsoft). Avec cette dernière vous pouvez :

1. changer de diapositive ou de page
2. zoomer et afficher en plein écran
3. ajouter des annotations

![](./BBB-images/2020-04-03_18-23.png)

## Contrôles audio et vidéo

Selon votre rôle vous aurez certaines icônes situées sur la présentation au milieu.

Selon les cas vous pouvez disposer de deux à quatre icônes.

* *Rendre silencieux*/*Autoriser à parler* : vous coupez juste votre micro mais vous pouvez le réactiver facilement. Seulement si vous avez activé votre micro.
* *Quitter l'audio*/*Rejoindre l'audio* : vous coupez le son, donc n'entendez plus rien. En rejoignant vous pouvez choisir d'être spectateur ou d'activer votre micro (cf. entrée dans le salon).
* *Partager webcam* : pour activer votre webcam et être vu des autres participants.
* *Partager votre écran* : seulement si vous êtes présentateur, vous pouvez partager une fenêtre particulière, un onglet du navigateur (Chrome et Chromium) ou tout votre écran.

![](./BBB-images/2020-04-03_18-46.png)

Si vous n'êtes que spectateur, vous verrez un casque à la place du combiné de téléphone. Vous pouvez toutefois partager votre webcam et votre écran (si vous êtes présentateur).

![](./BBB-images/2020-04-03_18-58.png)

## Bouton Actions

Dans le coin inférieur gauche de la présentation le bouton orné d'un signe plus donne accès aux actions de présentateur.

Les modérateurs ont toujours accès à ce bouton. Ils leur permet de devenir présentateur (le précédent perd aussitôt ce statut).

![](./BBB-images/2020-04-03_19-32.png)

La personne qui anime la présentation a alors accès à trois fonctions :

* *Débuter une sondage* : permet de recueillir rapidement l'avis de l'assistance en posant une question simple.
* *Charger une présentation* : pour ajouter et/ou changer de présentation.
* *Partager une vidéo externe* via son URL depuis un site de diffusion de vidéos tels que Youtube, Vimeo, Dailymotion, etc.

![](./BBB-images/2020-04-03_19-32_1.png)

## Menu options

Dans le coin supérieur droit se trouve un bouton 3 points ouvrant le menu des options.

À noter les fonctions pour quitter :

* *Mettre fin à la réunion* : le salon est fermé et tout le monde forcé à quitter. Évidemment, seul un modérateur a accès à cette fonction.
* *Déconnexion* : pour quitter le salon (le retour est possible).

![](./BBB-images/2020-04-03_19-41.png)

Les paramètres permettent de rajouter des alertes, changer la langue, augmenter la taille des caractères voire désactiver les webcams et partage de bureau.

![](./BBB-images/2020-04-03_19-55.png)


# Montrer du contenu

## Charger une présentation

Il y a toujours une présentation par défaut, paramétrée au niveau administrateur. Cependant, en tant que présentateur, vous pouvez en charger une nouvelle. Cliquez sur le bouton *Actions* et choisissez *Charger une présentation*.

![](./BBB-images/2020-04-13_20-36.png)

Dans la fenêtre qui s'ouvre, il vous suffit de faire glisser un document pour l'ajouter à la liste.

**Important** le format PDF est vivement conseillé. Cependant, vous pouvez utiliser un format de suite bureautique LibreOffice (odt, ods, odp) ou MS Office (docx, xslx, pptx). Vous ne pouvez pas utiliser de format image (jpg, png, etc.) et c'est bien dommage. Si vous voulez mettre en ligne une capture d'écran fraîchement réalisée au format png, vous devrez la convertir en PDF au préalable.

![](./BBB-images/2020-04-13_20-37.png)

À tout moment vous pouvez gérer les présentations chargées sur la plateforme et revenir sur une précédemment montrée. Dans la liste de présentations chacune est terminée par deux (celle par défaut) ou trois icônes.

* La première permet de rendre la présentation téléchargeable par les autres participants. Une icône de téléchargement apparaît alors en bas à gauche de la présentation.
* La deuxième permet de sélectionner celle affichée.
* La troisième permet de supprimer la présentation de la plateforme (à noter qu'à la fermeture du salon de discussion, seule la présentation par défaut subsiste).


## Tableau blanc

Il n'y a pas de fonction « tableau blanc », il faut utiliser une présentation avec des diapositives vierges.
La présentation par défaut comporte une diapositive de bienvenue ainsi que six diapositives vierges.

![](./BBB-images/2020-04-03_18-34.png)

## Annoter le tableau

Qu'il soit blanc ou pourvu d'une présentation, il est possible d'annoter le tableau comme on le ferait sur un vrai tableau physique. Notez que l'on ne peut pas annoter une vidéo ni un partage d'écran.

Lorsque vous êtes présentateur vous disposez d'une barre d'outils à droite de la présentation. Cliquez sur la première icône pour choisir un outil :

* Texte. Pour taper un texte vous devez d'abord tracer une zone rectangulaire dans lequel il sera circonscrit.
* Ligne droite.
* Ellipse.
* Triangle (limité à un triangle isocèle avec la pointe en haut).
* Rectangle.
* Dessin à main levée (avec assistance, le trait est adouci).
* *Pan* ou panoramique en bon français, l'outil Main vous permet de déplacer la vue lorsque la présentation est grossie et n'apparaît pas en entier dans l'écran.

Puis les autres boutons :

* choisir l'épaisseur du trait ou la taille des caractères ;
* choisir la couleur ;
* annuler le dernier tracé ;
* effacer toutes vos annotations.

Enfin, le dernier bouton active le mode multi-utilisateurs. Cela permet à tous les participants d'annoter également. Le curseur de chacun est identifié avec son nom.

![](./BBB-images/2020-04-16_16-09.png)

> ### Note
> les annotations sont propres à chaque vignette de la présentation. Les annotations ne sont pas conservées à la fermeture du salon et ne sont pas exportées lorsque l'on télécharge la présentation. Le seul moyen de les conserver est de faire des captures d'écran.

## Partager une vidéo

Au préalable vous devez avoir téléversé votre vidéo sur une plateforme de diffusion telle que Youtube, Dailymotion ou Vimeo, vous ne pouvez pas déposer directement votre vidéo dans BigBlueButton !

Voici un exemple avec une vidéo mise en ligne sur Vimeo. Sur la page diffusant la vidéo vous pouvez copier l'URL dans la barre d'adresse ou cliquer sur le bouton *Partager* et copier le lien (URL).

![](./BBB-images/2020-04-16_16-23.png)

Revenez ensuite dans BigBlueButton et, via le bouton *Actions* choisissez *Partager une vidéo externe*. Dans la nouvelle fenêtre collez le lien copié précédemment puis validez.

![](./BBB-images/2020-04-16_16-39.png)

Il faut ensuite que chaque participant démarre la lecture de la vidéo pour permettre la synchronisation de lecture. Lorsque le présentateur met en pause, déplace le curseur de temps cette information est répercutée chez tous les autres participants. Cependant, un participant peut faire de même de son côté sans que cela n'impacte les autres. Attention cependant, toute action du présentateur resynchronise la lecture.

## Partager son écran

Cela peut être utile, voire nécessaire de montrer ce que vous faites à l'écran, notamment si votre cours comporte une part de pratique en direct que vous souhaitez montrer. Le bouton *Partager votre écran* n'apparaît que si vous êtes présentateur.

Vous ouvrez ainsi une fenêtre vous proposant de :

* partager tout votre écran : si vous utilisez deux écrans, vous partagerez sans doute les deux, ce qui est une mauvaise idée car l'affichage sera trop petit chez les participants.
* partager une seule fenêtre logicielle : attention, les menus et fenêtres additionnelles telles que *Ouvrir un fichier* ne sont pas visibles.
* dans Chrome ou Chromium seulement, vous pouvez également partager un onglet de ce navigateur.

> ### Mon conseil
> 
> J'ai deux écrans et je souhaite montrer l'usage complet d'un logiciel : j'utilise une machine virtuelle créée avec Virtual Box et je partage cette fenêtre. Cela me permet en outre d'avoir un environnement non personnalisé.

![](./BBB-images/2020-04-17_12-47.png)

Cliquez à nouveau sur le bouton *Partager votre écran* pour stopper le partage et revenir à la présentation.

> ### Note
> 
> Attention, il s'agit juste de partager votre écran. Ce dernier sera vu comme une vidéo dans BigBlueButton. Il n'y a aucune possibilité de prendre la main.
> 
> Si vous cherchez une solution libre de prise de main à distance, essayez [Jitsi Meet Electron](https://github.com/jitsi/jitsi-meet-electron/).

# Interagir avec les participants

## Indiquer son ressenti

Outre le fait de vous exprimer via votre micro ou l'espace de discussion publique vous pouvez également donner des indications plus discrètes. Cela peut être utile s'il y a beaucoup de participants pour éviter de tourner à la cacophonie.

Dans la liste des utilisateurs à gauche cliquez sur votre nom. Un menu s'ouvre et vous pouvez *Définir votre statut*.

* *Éloigné* : signalez votre absence devant l'écran (aller aux toilettes, ouvrir au facteur, etc.)
* *Lever la main* : signalez votre volonté de parler.
* Indiquez votre ressenti : *indécis*, *désorienté*, *triste*, *ravi*, *applaudissements*.
* Le présentateur peut également vous demander votre avis, *favorable* ou *défavorable*, mais il est plus pratique de recueillir un avis via un sondage.

Notez que ces indications apparaissent sous forme d'icône à la place des deux lettres de votre prénom dans la liste des utilisateurs. Cela manque de visibilité. À réserver aux grands rassemblements.

![](./BBB-images/2020-04-17_19-06.png)

Le modérateur peut, à tout moment, réinitialiser ces ressentis via le menu des utilisateurs (icône rouage puis *Effacer les icônes de statut*).

## Soumettre un sondage

Vous pouvez poser des questions aux participants en organisant les réponses sous la forme d'un sondage. Cela permet d'obtenir une réponse de tout le monde.

Depuis le bouton *Actions* vous pouvez *débuter un sondage*.

![](./BBB-images/2020-04-17_20-01.png)

Les choix peuvent sembler limités et surtout, c'est forcément un questionnaire à choix multiples (QCM), vous ne pouvez poser de question ouverte par ce biais. La question est posée oralement ou dans le canal de discussion publique.

Voici un exemple ou vous soumettez un choix de menu pour la pause méridienne (ce qui n'a pas beaucoup de sens vu qu'à priori, les participants sont tous éloignés les uns des autres).

1. Définissez les réponses (ici c'est un sondage personnalisé, donc, vous devez les rédiger).
2. Démarrez le sondage, une fenêtre avec les réponses s'affiche sur les écrans des participants.
3. Suivez les réponses au fur et à mesure et vérifiez que tout le monde répond.
4. Enfin, vous pouvez choisir de publier ou non les réponses auprès des participants.

![](./BBB-images/2020-04-17_20-30.png)

## Réunions de travail

BigBlueButton peut également être utilisé pour une réunion de travail, sans présentation support, simplement pour se voir et discuter.

* Fermez la présentation si vous ne l'utilisez pas, cela augmente la place allouée aux caméras.
* Chaque participant peut activer sa caméra s'il le souhaite (ou s'il le peut).
* Une ou deux personnes animent la réunion. Les autres participants n'allument leur micro qu'au moment de parler.
* Il est possible, voire conseillé de prendre des notes partagées.
* L'espace de discussion publique peut également être utilisé.

![](./BBB-images/2020-04-16_19-57.png)

## Contrôle des micros

Un modérateur peut couper le micro d'un participant en cliquant sur la pastille portant son nom située au-dessus des webcams. Cette pastille n'apparaît que lorsque le micro de la personne capte du son.

On peut également couper le micro de quelqu'un en cliquant sur son nom dans la liste des utilisateurs puis *Rendre muet*.

![](./BBB-images/2020-04-17_19-35.png)

Un modérateur peut également mettre tout le monde en sourdine (éventuellement sauf le présentateur) via le menu des utilisateurs.

## Sous-groupes

Il est possible de créer des « réunions privées ». Ce sont des espaces offrant les mêmes caractéristiques que le salon principal permettant à des participants de se regrouper par petits groupes, sans se déconnecter du salon principal.

Pour cela, ouvrez le menu utilisateurs et choisissez *Créer des réunions privées*.

![](./BBB-images/2020-04-17_20-45.png)

* Vous pouvez créer entre deux et huit réunions privées.
* Vous devez fixer une durée d'existence de ces espaces de réunion.
* Vous devez ensuite faire glisser à la souris des participants dans au moins un espace de réunion.
* Par défaut, les participants ne peuvent aller que dans la réunion dans laquelle vous les avez affectés. La case à cocher *Autoriser les participants à choisir une salle de réunion à rejoindre* permet de laisser libres les mouvements de salle.

![](./BBB-images/2020-04-17_20-54.png)

Chaque personne assignée à une réunion privée reçoit ensuite une invitation. Accepter cette invitation ajoute la personne à l'espace privé **et** coupe également son micro dans le salon principal. Il est alors à nouveau demandé de choisir entre utiliser le micro ou être seulement spectateur.

Ensuite, tout se passe comme dans le salon principal.

![](./BBB-images/2020-04-17_20-59.png)

Depuis le salon principal une nouvelle entrée est ajoutée au menu de gauche permettant de rejoindre une réunion privée. Si vous êtes modérateur, vous pouvez aller dans n'importe quelle salle. Vous pouvez également clôturer toutes les réunions.

![](./BBB-images/2020-04-17_21-10.png)

> ### Quelques précautions à prendre
> 
> * Les groupes de réunion privée sont matérialisés par de nouveaux onglets dans le navigateur : vous pouvez donc basculer à nouveau sur le salon principal pour voir ce qu'il s'y passe, éventuellement inviter quelqu'un à rejoindre un groupe privé.
> * Attention à votre micro : en revenant sur le salon principal vous pouvez réactiver votre micro. Vous parlerez alors à la fois dans le salon principal **et** dans le groupe privé (ou comment inventer l'ubiquité numérique). Pensez à couper le micro dans la salle où vous ne voulez plus intervenir.
> * De même si vous rejoignez un autre groupe privé.

## Discussion privée avec une personne

Il est possible d'engager une discussion écrite privée avec quelqu'un. Dans la liste des utilisateurs cliquez sur le nom de la personne avec laquelle vous souhaitez communiquer et choisissez *Démarrer une discussion privée*.

![](./BBB-images/2020-04-18_13-04.png)

Commencez à rédiger votre conversation et le destinataire sera notifié.

![](./BBB-images/2020-04-18_13-09.png)

Il ne lui reste qu'à cliquer sur la discussion dans le menu *Messages* pour l'ouvrir et répondre.

![](./BBB-images/2020-04-18_13-11.png)

# Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.

[Version PDF](./bbb.pdf) — [Version Markdown](./bbb.md)
